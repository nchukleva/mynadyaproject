package com.example.springprimes.controllers;

import com.example.springprimes.controllers.requests.NumberRequest;
import com.example.springprimes.services.NumberService;
import com.example.springprimes.services.PrimeNumberGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "print")
public class NumbersController {

    private NumberService numberActionService;

    @Autowired
    public NumbersController(@Qualifier("primeNumbers") NumberService numberActionService) {
        this.numberActionService = numberActionService;
    }

    @GetMapping
    public List<Integer> print(@RequestParam(name = "startNumber") Integer start,
                                @RequestParam(name = "endNumber") Integer end) {

        NumberRequest number = new NumberRequest(start, end);

        System.out.println(number);

        return numberActionService.doAction(number);
    }

    @PostMapping
    public List<Integer> printNum(@RequestBody NumberRequest numberRequest) {

        PrimeNumberGeneratorService primes = new PrimeNumberGeneratorService();

        System.out.println(numberRequest);

        return primes.doAction(numberRequest);
    }


}
