package com.example.springprimes.services;

import com.example.springprimes.controllers.requests.NumberRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("primeNumbers")
public class PrimeNumberGeneratorService extends BaseNumberServiceImpl {

    @Override
    public List<Integer> doAction(NumberRequest number) {

        ArrayList<Integer> primes = new ArrayList<>();

        for (int numberToCheck = number.getStartNumber(); numberToCheck <= number.getEndNumber(); numberToCheck++) {
            boolean isPrime = true;

            int divider = 2;
            while (divider <= numberToCheck / 2) {
                if (numberToCheck % divider == 0) {
                    isPrime = false;
                    break;
                }
                divider++;
            }

            if (isPrime) {
                primes.add(numberToCheck);
            }
        }

        return primes;
    }
}
