package com.example.springprimes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringPrimesApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringPrimesApplication.class, args);
    }

}
