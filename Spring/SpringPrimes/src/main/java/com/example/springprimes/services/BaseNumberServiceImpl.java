package com.example.springprimes.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

public abstract class BaseNumberServiceImpl implements NumberService {

    @Override
    public void printToConsole(List<Integer> numbers) {
        System.out.println(numbers);
    }

    @Override
    public void printToFile(List<Integer> numbers) throws IOException {
        Instant instant = Instant.now().truncatedTo(ChronoUnit.SECONDS);
        String output = instant
                .toString()
                .replace(":", "_");
        FileWriter fileWriter = new FileWriter(output + ".output.txt");
        BufferedWriter writer = new BufferedWriter(fileWriter);

        writer.write(String.valueOf(numbers));
        writer.close();
    }
}
