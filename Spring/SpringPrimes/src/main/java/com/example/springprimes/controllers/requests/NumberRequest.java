package com.example.springprimes.controllers.requests;

public class NumberRequest {

    private int startNumber;
    private int endNumber;

    public NumberRequest(int startNumber, int endNumber) {
        this.startNumber = startNumber;
        this.endNumber = endNumber;
    }

    public int getStartNumber() {
        return startNumber;
    }

    public int getEndNumber() {
        if (this.endNumber <= this.startNumber) {
            throw new ArithmeticException("End integer must be bigger than start integer");
        }

        return this.endNumber;
    }

    @Override
    public String toString() {
        return "NumberRequest{" +
                "startNumber=" + startNumber +
                ", endNumber=" + endNumber +
                '}';
    }
}
