package com.example.springprimes.services;

import com.example.springprimes.controllers.requests.NumberRequest;

import java.io.IOException;
import java.util.List;

public interface NumberService {

    List<Integer> doAction(NumberRequest number);

    void printToConsole(List<Integer> numbers);

    void printToFile(List<Integer> numbers) throws IOException;
}
