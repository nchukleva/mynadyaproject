package com.example.springprimes.services;

import com.example.springprimes.controllers.requests.NumberRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service("randomNumbers")
public class RandomNumberGeneratorService extends BaseNumberServiceImpl {

    @Override
    public List<Integer> doAction(NumberRequest number) {

        Random random = new Random();
        List<Integer> result = new ArrayList<>(number.getEndNumber());

        for (int i = number.getStartNumber(); i < number.getEndNumber(); i++) {
            result.add(random.nextInt());
        }

        return result;
    }
}
