package OnlineStore;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        //with constructor
        Order order1 = new Order("John", Currency.BGN, LocalDate.now());

        order1.items.add(new Product("Keyboard", 50));
        order1.items.add(new Product("Monitor", 1000));
        //without constructor
        /*order1.recipient = "John";
        order1.totalPrice = 400;
        order1.currency = "BGN";
        order1.deliveryDate = LocalDate.now();
        order1.items = new String[] {"PC", "Keyboard", "Mouse"};*/

        Order order2 = new Order("Frank", Currency.EUR, LocalDate.now());
        order2.items.add(new Product("T-Shirt", 10));
        order2.items.add(new Product("Jeans", 20));

        //without constructor
        /*order2.recipient = "Frank";
        order2.totalPrice = 120;
        order2.currency = "EUR";
        order2.deliveryDate = LocalDate.now();
        order2.items = new String[] {"Jeans", "T-shirt"};*/

        Order[] orders = new Order[] {order1, order2};

        for (Order order : orders) {
            //without methods
            /* System.out.printf("To be delivered on: %s %n", order.deliveryDate.toString());
            for (String item : order.items) {
                System.out.printf("%s ", item);
            }
            System.out.printf("%n----------------------%n");*/

            //with methods
            order.displayGeneralInfo();
            System.out.println();
            order.displayOrderItems();
            System.out.printf("%n----------------------%n");
            }
        }
    }
