package OnlineStore;

import java.time.LocalDate;
import java.util.ArrayList;

public class Order {
    String recipient;
    //before product class
    //double totalPrice;
    //String currency;
    Currency currency;
    LocalDate deliveryDate;

    //String[] items
    ArrayList<Product> items;

    //with constructor
    public Order(String recipient, //double totalPrice,
                 Currency currency, LocalDate deliveryDate) {
        this.recipient = recipient;
        //this.total = total;
        this.currency = currency;
        this.deliveryDate = deliveryDate;
        //this.items = items;
        items = new ArrayList<>();
    }

    //with methods
    public void displayOrderItems () {
        if (items.size() == 0) {
            System.out.println("No items");
        }

        StringBuilder sb = new StringBuilder("Items: ");
        for (Product item : items) {
            sb.append(String.format("%s ", item.getDisplayInfo()));
        }

        System.out.println(sb.toString());
    }
    //with methods
    public void displayGeneralInfo () {
        System.out.printf("Order to: %s | Delivery on: %s | Total: %s", recipient, deliveryDate, calculateTotalPrice());
    }

    public double calculateTotalPrice () {
        double total = 0;

        for (Product product : items) {
            total += product.price;
        }

        if (currency == Currency.EUR) {
            total = total * 2;
        }
        return total;
    }
}
