package ForumPost;

import java.util.ArrayList;
import java.util.List;

public class ForumPost {
    public String author;
    public String text;
    public int upVotes;
    public ArrayList<String> replies;

    public ForumPost (String author, String text) {
        this(author, text, 0);
    }

    public ForumPost (String author, String text, int upVotes) {
        this.author = author;
        this.text = text;
        this.upVotes = upVotes;
        replies = new ArrayList<>();
    }

    public String format () {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s / by %s, %d votes. %n", text, author, upVotes));
        for (String reply : replies) {
            sb.append(String.format("- %s", reply)).append(System.lineSeparator());
        }
        return sb.toString();
    }


}
