package PrimesOOP;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class Numbers implements NumberActions {

    private int startNumber;
    private int endNumber;

    public Numbers(int startNumber, int endNumber) {
        this.startNumber = startNumber;
        this.endNumber = endNumber;
    }

    public int getStartNumber() {
        return startNumber;
    }

    public int getEndNumber() {
        return endNumber;
    }

    @Override
    public void checkInput(int startNumber, int endNumber) {
        if (endNumber <= startNumber) {
                System.out.println("End integer must be bigger than start integer");
                return;
        }
    }

    @Override
    public List<Integer> findPrimes (int startNumber, int endNumber) {

        checkInput(startNumber, endNumber);

        ArrayList<Integer> primes = new ArrayList<>();

        for (int numberToCheck = startNumber; numberToCheck <= endNumber; numberToCheck++) {
            boolean isPrime = true;

            int divider = 2;
            while (divider <= numberToCheck / 2) {
                if (numberToCheck % divider == 0) {
                    isPrime = false;
                    break;
                }
                divider++;
            }

            if (isPrime) {
                primes.add(numberToCheck);
            }
        }

        return primes;
    }

    @Override
    public void printPrimesToConsole(int startNumber, int endNumber) {
        List<Integer> primes = findPrimes (startNumber, endNumber);
        System.out.println(primes);
    }

    @Override
    public void printPrimesToFile(int a, int b) throws IOException {
        Instant instant = Instant.now().truncatedTo(ChronoUnit.SECONDS);
        String output = instant
                .toString()
                .replace(":", "_");
        FileWriter fileWriter = new FileWriter(output + ".output.txt");
        BufferedWriter writer = new BufferedWriter(fileWriter);

        List<Integer> primes = findPrimes (startNumber, endNumber);
        writer.write(String.valueOf(primes));
        writer.close();
    }
}
