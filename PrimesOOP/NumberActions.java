package PrimesOOP;

import java.io.IOException;
import java.util.List;

public interface NumberActions {

    void checkInput(int a, int b);

    List<Integer> findPrimes(int a, int b);

    void printPrimesToConsole(int a, int b);

    void printPrimesToFile(int a, int b) throws IOException;
};




