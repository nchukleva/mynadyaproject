package PrimesOOP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int startNumber;
        int endNumber;

        try {
            System.out.println("Enter start integer:");
            startNumber = Integer.parseInt(reader.readLine());

            System.out.println("Enter end integer:");
            endNumber = Integer.parseInt(reader.readLine());
        } catch (NumberFormatException e) {
            System.out.println("Enter a valid integer");
            return;
        }

        Numbers numbers = new Numbers(startNumber, endNumber);
        //numbers.printPrimesToConsole(numbers.getStartNumber(), numbers.getEndNumber());
        numbers.printPrimesToFile(numbers.getStartNumber(), numbers.getEndNumber());

    }
}
