import java.io.*;
import java.util.ArrayList;

public class findPrimesFromFileToConsole {
    public static void main(String[] args) throws IOException {

        try {
            BufferedReader br = readInput();
            int startNum = Integer.parseInt(br.readLine());
            int endNum = Integer.parseInt(br.readLine());

            System.out.print(findPrimes(startNum, endNum));

        } catch (IOException e) {
            System.out.println("Error. Write a start and end number on separate lines.");
        }
    }

    private static BufferedReader readInput() throws FileNotFoundException {
        String path = "C:\\Users\\Nadya\\IT\\Java\\JavaExercises\\FindingPrimes\\input.txt";
        FileInputStream fis = new FileInputStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        return br;
    }

    public static ArrayList<Integer> findPrimes(int startNum, int endNum) {

        ArrayList<Integer> primes = new ArrayList<>();

        for (int numberToCheck = startNum; numberToCheck <= endNum; numberToCheck++) {
            boolean isPrime = true;

            int divider = 2;
            while(divider <= numberToCheck / 2) {
                if(numberToCheck % divider == 0) {
                    isPrime = false;
                    break;
                }
                divider++;
            }

            if(isPrime) {
                primes.add(numberToCheck);
            }
        }

        return primes;
    }
}
