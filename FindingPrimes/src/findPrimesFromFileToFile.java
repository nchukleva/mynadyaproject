import java.io.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

public class findPrimesFromFileToFile {
    public static void main(String[] args) throws IOException {

        try {
            BufferedReader br = readInput();
            int startNum = Integer.parseInt(br.readLine());
            int endNum = Integer.parseInt(br.readLine());

            ArrayList<Integer> primes = findPrimes(startNum, endNum);

            writeOutput(primes);

        } catch (IOException e) {
            System.out.println("Error. Write a start and end number.");
        }
    }

    private static void writeOutput(ArrayList<Integer> primes) throws IOException {
        FileWriter fileWriter = new FileWriter("output.txt");
        BufferedWriter bw = new BufferedWriter(fileWriter);
        String timestamp = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss").format(new Date());

        for (Integer number : primes) {
            bw.write(number.toString() + System.lineSeparator());
        }
        bw.write(timestamp);
        bw.close();
    }

    private static BufferedReader readInput() throws IOException {
        String path = "C:\\Users\\Nadya\\IT\\Java\\JavaExercises\\FindingPrimes\\input.txt";
        FileInputStream fis = new FileInputStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        return br;
    }

    public static ArrayList<Integer> findPrimes(int startNum, int endNum) {

        ArrayList<Integer> primes = new ArrayList<>();

        for (int numberToCheck = startNum; numberToCheck <= endNum; numberToCheck++) {
            boolean isPrime = true;

            int divider = 2;
            while(divider <= numberToCheck / 2) {
                if(numberToCheck % divider == 0) {
                    isPrime = false;
                    break;
                }
                divider++;
            }

            if(isPrime) {
                primes.add(numberToCheck);
            }
        }

        return primes;
    }
}
